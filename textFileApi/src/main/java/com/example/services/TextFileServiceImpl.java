package com.example.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.example.TextFileApiApplication;
import com.example.model.AuxWordComparable;

@Service
public class TextFileServiceImpl implements TexFileService{

	@Override
	public List<String> getNMostUsedWord(int n)  {
	    Map<String, Integer> wordsMap = null;
		try {
			wordsMap = wordAndCounters();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (wordsMap!=null) {
			return FindMaxOccurance(wordsMap, n);
		}
		return new ArrayList<>();
	}

	@Override
	public int ocurrenceOfAWord(String word) {
		int counter = 0;
		try {
			Map<String, Integer> wordsMap = wordAndCounters();
			if (wordsMap!=null) {
				counter = wordsMap.get(word);
			}
		
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return counter;
	}
	
	private Map<String, Integer> wordAndCounters() throws Exception{
		
		File rootFolder = new File(TextFileApiApplication.ROOT);
		List<String> fileNames = Arrays.stream(rootFolder.listFiles())
			.map(f -> f.getName())
			.collect(Collectors.toList());
		
		Map<String, Integer> wordsMap = new HashMap<>();
		for (String file : fileNames) {
			BufferedReader bufferedReader = null;
			try {
				bufferedReader = new BufferedReader(new FileReader(TextFileApiApplication.ROOT+File.separator+file));
				String inputLine = null;
			    while ((inputLine = bufferedReader.readLine()) != null) {
					String[] words = inputLine.split("[ \n\t\r.,;:!?(){}]");
	 
					for (int counter = 0; counter < words.length; counter++) {
						String key = words[counter].toLowerCase(); // remove .toLowerCase for Case Sensitive result.
						if (key.length() > 0) {
							if (wordsMap.get(key) == null) {
								wordsMap.put(key, 1);
							} else {
								int value = wordsMap.get(key).intValue();
								value++;
								wordsMap.put(key, value);
							}
						}
					}
				}
				
				
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				bufferedReader.close();
			}
		
		}
		return wordsMap;
	}
	
	public static List<String> FindMaxOccurance(Map<String, Integer> map, int n) {
		List<AuxWordComparable> listComp = new ArrayList<>();
		for (Map.Entry<String, Integer> entry : map.entrySet())
			listComp.add(new AuxWordComparable(entry.getKey(), entry.getValue()));
 
		Collections.sort(listComp);
		List<String> list = new ArrayList<>();
		if (n >= listComp.size()) {
			n=listComp.size();
		}
		for (AuxWordComparable w : listComp.subList(0, n))
			list.add(w.wordFromFile);
		return list;
	}
	
	
}

