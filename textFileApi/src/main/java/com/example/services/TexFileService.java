package com.example.services;

import java.util.List;

public interface TexFileService {

	List<String> getNMostUsedWord(int n);
	int ocurrenceOfAWord(String word);
}
