package com.example.api;

import java.util.ArrayList;
import java.util.List;

import javax.websocket.server.PathParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.services.TexFileService;

@RestController
public class TextFileApi {
	
	@Autowired
	private TexFileService serviceText;

	
	@RequestMapping("/getmostuserword")
	public List<String> getMostUsedWord(@RequestParam(value="n")Integer n ){
		try {
			return serviceText.getNMostUsedWord(n);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new ArrayList<>();
	}
	
	@RequestMapping("/wordocurrence")
	public int wordocurrence(@PathParam(value="word")String word ){
		return serviceText.ocurrenceOfAWord(word);
	}
	
	public TexFileService getServiceText() {
		return serviceText;
	}

	public void setServiceText(TexFileService serviceText) {
		this.serviceText = serviceText;
	}
	
	
}
