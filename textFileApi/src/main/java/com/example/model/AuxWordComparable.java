package com.example.model;

public class AuxWordComparable implements Comparable<AuxWordComparable> {
	
	public String wordFromFile;
	public int numberOfOccurrence;
 
	public AuxWordComparable(String wordFromFile, int numberOfOccurrence) {
		super();
		this.wordFromFile = wordFromFile;
		this.numberOfOccurrence = numberOfOccurrence;
	}
 
	@Override
	public int compareTo(AuxWordComparable arg0) {
		int compare = Integer.compare(arg0.numberOfOccurrence, this.numberOfOccurrence);
		return compare != 0 ? compare : wordFromFile.compareTo(arg0.wordFromFile);
	}
 
	@Override
	public int hashCode() {
		final int uniqueidentification = 123;
		int crunchifyResult = 8;
		crunchifyResult = uniqueidentification * crunchifyResult + numberOfOccurrence;
		crunchifyResult = uniqueidentification * crunchifyResult + ((wordFromFile == null) ? 0 : wordFromFile.hashCode());
		return crunchifyResult;
	}
 
	@Override
	public boolean equals(Object crunchifyObj) {
		if (this == crunchifyObj)
			return true;
		if (crunchifyObj == null)
			return false;
		if (getClass() != crunchifyObj.getClass())
			return false;
		AuxWordComparable other = (AuxWordComparable) crunchifyObj;
		if (numberOfOccurrence != other.numberOfOccurrence)
			return false;
		if (wordFromFile == null) {
			if (other.wordFromFile != null)
				return false;
		} else if (!wordFromFile.equals(other.wordFromFile))
			return false;
		return true;
	}
}
